package ru.tsc.gavran.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.command.AbstractProjectCommand;
import ru.tsc.gavran.tm.enumerated.Role;
import ru.tsc.gavran.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.gavran.tm.model.Project;
import ru.tsc.gavran.tm.util.TerminalUtil;

public class ProjectStartByNameCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-start-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Start project by name.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectService().startByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        serviceLocator.getProjectService().startByName(userId, name);
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}