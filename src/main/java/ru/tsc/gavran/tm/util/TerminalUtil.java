package ru.tsc.gavran.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gavran.tm.exception.AbstractException;
import ru.tsc.gavran.tm.exception.system.IndexIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() throws AbstractException {
        @NotNull final String value = SCANNER.nextLine();
        try {
            return Integer.parseInt(value);
        } catch (@NotNull RuntimeException e) {
            throw new IndexIncorrectException(value);
        }
    }

    static void incorrectValue() {
        System.out.println("Incorrect value");
    }

}